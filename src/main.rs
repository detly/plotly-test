use plotly::{
    common,
    layout::{self, GridPattern},
};

fn main() -> std::io::Result<()> {
    let times = vec![1, 2, 3, 4];
    let ones = vec![1, 2, 3, 4];
    let twos = vec![2, 4, 8, 16];
    let threes = vec![4, 3, 2, 1];

    let plot_one = plotly::Scatter::new(times.clone(), ones)
        .mode(common::Mode::LinesMarkers)
        .line(common::Line::new().shape(common::LineShape::Hv))
        .name("ONE");

    let plot_two = plotly::Scatter::new(times.clone(), twos)
        .mode(common::Mode::LinesMarkers)
        .line(common::Line::new().shape(common::LineShape::Hv))
        .name("TWO")
        .x_axis("x2")
        .y_axis("y2");

    let plot_three = plotly::Scatter::new(times, threes)
        .mode(common::Mode::LinesMarkers)
        .line(common::Line::new().shape(common::LineShape::Hv))
        .name("THREE")
        .x_axis("x3")
        .y_axis("y3");

    let layout = layout::Layout::new()
        .grid(
            layout::LayoutGrid::new()
                .rows(3)
                .columns(1)
                .pattern(GridPattern::Independent),
        )
        .title("Data".into())
        .y_axis(layout::Axis::new().title("ONE".into()))
        .y_axis2(layout::Axis::new().title("TWO".into()))
        .y_axis3(layout::Axis::new().title("THREE".into()));

    let mut plot = plotly::Plot::new();
    plot.add_trace(plot_one);
    plot.add_trace(plot_two);
    plot.add_trace(plot_three);
    plot.set_layout(layout);
    plot.show();

    Ok(())
}
